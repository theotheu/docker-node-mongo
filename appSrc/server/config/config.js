module.exports = {
    development: {
        db: 'mongodb://mongo:27017/books',      // change books with your database
        port: 3000,                             // change 3000 with your port number
        debug: true                             // set debug to true|false
    }, test: {

    }, production: {

    }
};
