# https://docs.docker.com/engine/reference/builder/
FROM node

RUN mkdir -p /app

ADD ./appSrc/ /app

WORKDIR /app/server

RUN npm install

COPY ./appSrc/. /app

RUN npm install -g nodemon

EXPOSE 3000

CMD ["npm", "start"]
