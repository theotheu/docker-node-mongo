@see https://medium.com/@sunnykay/docker-development-workflow-node-express-mongo-4bb3b1f7eb1e#.8b4bjh2b4


# Stop, rebuild  and start app
`docker ps -a | grep Exit | cut -d ' ' -f 1 | xargs docker rm && docker-compose up --build`

# Stop all containers
`docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)`

# Remove all containters
`docker rmi -f $(docker images -a -q)`

# BIG file on Mac OS X
```bash
docker rm $(docker ps -a -q)
docker rmi $(docker images -a -q)
docker volume rm $(docker volume ls |awk '{print $2}')
rm -rf ~/Library/Containers/com.docker.docker/Data/*
```

